package com.epam.bank;

public class NotEnoughMoneyException extends RuntimeException {

	public NotEnoughMoneyException(String message) {
		super(message);
	}
}
