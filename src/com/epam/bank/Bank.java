package com.epam.bank;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank {

	private volatile int moneyAmount;
	private final Lock bankLock;

	public Bank(int moneyAmount) {
		this.moneyAmount = moneyAmount;
		this.bankLock = new ReentrantLock();
	}

	public void transferMoney(int amount) {
		int withdrawResult = moneyAmount - amount;
		if (withdrawResult >= 0) {
			moneyAmount = withdrawResult;
		} else {
			throw new NotEnoughMoneyException("Not enough money in bank." +
					" Withdraw " + amount + " moneyAmount " + moneyAmount);
		}
	}

	public boolean hasMoney(int amount) {
		return moneyAmount >= amount;
	}

	public Lock getBankLock() {
		return bankLock;
	}

	@Override
	public String toString() {
		return "Bank moneyAmount " + moneyAmount + '.';
	}
}
