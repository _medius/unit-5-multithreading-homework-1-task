package com.epam.bank;

public class Main {

	private static final int NUMBER_OF_BANK_USERS = 4;
	private static final int MONEY_AMOUNT = 4000;

	private Bank bank = new Bank(MONEY_AMOUNT);

	private void appStart() {
		for (int i = 0; i < NUMBER_OF_BANK_USERS; i++) {
			new Thread(new BankUser(bank)).start();
		}
	}

	public static void main(String[] args) {
		new Main().appStart();
	}
}
