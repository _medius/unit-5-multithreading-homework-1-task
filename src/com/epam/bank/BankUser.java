package com.epam.bank;

import java.util.concurrent.TimeUnit;

public class BankUser implements Runnable {

	private static final int WITHDRAW_AMOUNT = 150;
	private static final int TIMEOUT = 500;
	private static int idSequence = 0;

	{
		id = ++idSequence;
	}

	private final int id;
	private final Bank bank;

	public BankUser(Bank bank) {
		this.bank = bank;
	}

	private void takeAsleep() {
		try {
			TimeUnit.MILLISECONDS.sleep(TIMEOUT);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		boolean withdrawSuccessful;
		do {
			withdrawSuccessful = false;

			takeAsleep();

			try {
				bank.getBankLock().lock();
				if (bank.hasMoney(WITHDRAW_AMOUNT)) {
					System.out.println(this + " try to withdraw " + WITHDRAW_AMOUNT + " from bank.");
					bank.transferMoney(WITHDRAW_AMOUNT);
					withdrawSuccessful = true;
				}
			} finally {
				bank.getBankLock().unlock();
			}
		} while (withdrawSuccessful);

		System.out.println(this + " has finished his work. " + bank);
	}

	@Override
	public String toString() {
		return "BankUser " + id;
	}
}
