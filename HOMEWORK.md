# Multithreading Homework

1. Variant without any synchronization in initial commit 
(master (root-commit) 302fe8e). 
An error occurs because another thread can modify moneyAmount between 
call methods hasMoney() and transferMoney() by current thread. So 
hasMoney() returns true but when current thread call transferMoney()
in fact Bank may nit have enough money.
2. Solution with standard synchronization in second commit (master 
e0abf1b).
3. Solution with java.util.concurrent in third commit (master f42367f). 
Fourth commit just adds this readme. 
